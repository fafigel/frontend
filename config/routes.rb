Rails.application.routes.draw do

  get 'rates/index'

  get 'questions/FAQ'
  get 'calculator/cal'
  
 
  resources :questions
  resources :rates
 
  root 'rates#index'



end
