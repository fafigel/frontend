# rates_rails_client

## How To Configure the Rates APP for use
1. The client works with a rails restful API hence the *backend* should be cloned, configured and ran first.
1. Then clone the repositories frontend repository onto your local machine.
2. Open terminal/gitbash from both the location of the apps.
3. Run Bundle install to make sure all the gems are installed correctly.
3. Start the servers.
4. The server for the API(backend) has been configured to port 3001 so there wont be port conficts upon running command "rails s".
5. After starting the applications, open your browser and go to the url "http://localhost:3000/" thats where the index page is located.
6. On the URL you then find links to the *RATES* section and the *CALCULATOR* section in the navigation bar for easy access.
7. The rates are picked and displayed on the site direclty from the API.
8. The calcuator has a few modifications to be made for it to compute using rates directly from the API.






